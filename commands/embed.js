module.exports = {
    name: "евал",
    description: "Кодим кек",
    aliases: ["eval", "run"],
    category: "Владельцы",
    usage: "<код>",
    args: true,
    ownerOnly: true,
    async execute(message, args) {
        try {
          const result = await eval(args.join(' ')) // результат
          const evaluated = require('util').inspect(result, { depth: 0 });
          return message.reply(evaluated, { code: "js"})
        } catch (err) {
            return message.reply(err, { code: "js"})
        }
    }
  };