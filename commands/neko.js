const fetch = require('node-fetch')
const { MessageAttachment } = require('discord.js')
module.exports = {
    name: "неко",
    category: "NSFW",
    description: "Получить NSFW картинку с неко.",
    execute(message, args) {
        if (!message.channel.nsfw) return message.reply("используйте эту команду только в NSFW-каналах!")
        fetch('https://waifu.pics/api/nsfw/neko')
            .then(res => res.json())
            .then(json => message.channel.send(new MessageAttachment(json.url)))
    },
  };