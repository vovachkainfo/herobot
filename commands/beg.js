const { MessageEmbed } = require('discord.js')
module.exports = {
    name: "попрошайничать",
    category: "Экономика",
    description: "Попрошайничать.",
    args: false,
    guildOnly: true,
    async execute (message, args, mongoCurrency, db) {
        if (message.author.beggedToday == true) return;
        const randomMoney = Math.floor(Math.random() * 29) + 1
        await mongoCurrency.giveCoins(message.author.id, message.guild.id, randomMoney)
        const Embed = new MessageEmbed()
        .setAuthor(message.author.tag)
        .setDescription(`Вы попрошайничали на улице и получили ${randomMoney}₽`)
        message.channel.send(Embed)
        message.author.beggedToday = true
        setTimeout(function () {message.author.beggedToday = false}, 86400000)
    }
}