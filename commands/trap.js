const fetch = require('node-fetch')
const { MessageAttachment } = require('discord.js')
module.exports = {
    name: "трап",
    category: "NSFW",
    description: "Получить NSFW картинку с трапом.",
    execute(message, args) {
        if (!message.channel.nsfw) return message.reply("используйте эту команду только в NSFW-каналах!")
        fetch('https://waifu.pics/api/nsfw/trap')
            .then(res => res.json())
            .then(json => message.channel.send(new MessageAttachment(json.url)))
    },
  };