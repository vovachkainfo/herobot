module.exports = {
    name: "переместить",
    description: "Переместить друга к себе в канал",
    category: "Голосовые",
    usage: "<упоминание>",
    args: true,
    ownerOnly: true,
    async execute(message, args) {
        if(!message.mentions.users.first()) return message.reply("Пожалуйста, упомяните человека которого вы хотите переместить к себе!")
        let member = message.guild.members.cache.get(message.mentions.users.first().id)
        if (member.user.bot) return message.reply("Эй! Не перемещай к себе ботов!")
        if (!message.member.voice.channelID) return message.reply("Пожалуйста, войдите в голосовой канал.")
        if (!member.voice.channelID) return message.reply("Я не могу переместить человека, который не находится в голосовом канале.")
        member.voice.setChannel(message.member.voice.channelID)
    }
  };