module.exports = {
    name: "очистить",
    category: "Администрация",
    usage: "<число>",
    description: "Удалить указанное количество сообщений.",
    args: true,
    permission: "MANAGE_MESSAGES",
    execute(message, args) {
        message
        .delete()
        .then(() => {
          message.channel.bulkDelete(args[0]).then((messages) => {
            message.channel
              .send(`Удалил ${messages.size} сообщений.`)
              .then((botMessage) => {
                setTimeout(function () {
                  botMessage.delete();
                }, 3000);
              });
          });
        })
        .catch(console.error);
    },
  };