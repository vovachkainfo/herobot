const { prefix } = require("../config.json");
const { version } = require("../package.json");
const { MessageEmbed } = require("discord.js");
const { owners } = require("../config.json")
module.exports = {
  name: "хелп",
  aliases: ["команды"],
  usage: "[комманда]",
  description: "Все команды.",
  execute(message, args) {
    const { commands } = message.client;

    if (!args.length) {
      const Embed = new MessageEmbed()
        .setTitle("Хелпа")
        .setDescription("Все команды:")
        .addFields([
          {
            name: "Картинки",
            value: `\`${
              commands
                .filter((cmd) => cmd.category == "Картинки")
                .map((cmd) => cmd.name)
                .join(", ") || "Нет команд"
            }\``,
          },
          {
            name: "Информация",
            value: `\`${
              commands
                .filter((cmd) => cmd.category == "Информация")
                .map((cmd) => cmd.name)
                .join(", ") || "Нет команд"
            }\``,
          },
          {
            name: "Уровни",
            value: `\`${
              commands
                .filter((cmd) => cmd.category == "Уровни")
                .map((cmd) => cmd.name)
                .join(", ") || "Нет команд"
            }\``,
          },
          {
            name: "Экономика",
            value: `\`${
              commands
                .filter((cmd) => cmd.category == "Экономика")
                .map((cmd) => cmd.name)
                .join(", ") || "Нет команд"
            }\``,
          },
        ])
        .setFooter(
          `Вы можете написать ${prefix}help [комманда] чтобы получить информацию об этой комманде! | HeroBot v${version}`
        );
    if(message.member.hasPermission("ADMINISTRATOR")) Embed.addField("Администрация", `\`${commands.filter((cmd) => cmd.category == "Администрация").map(cmd => cmd.name).join(`, `) || "Нет команд"}\``)
    if(owners.includes(message.author.id)) Embed.addField("Владельцам", `\`${commands.filter((cmd) => cmd.category == "Владельцы").map(cmd => cmd.name).join(`, `) || "Нет команд"}\``)
    if(message.channel.nsfw) Embed.addField("NSFW", `\`${commands.filter((cmd) => cmd.category == "NSFW").map((cmd) => cmd.name).join(", ") || "Нет команд"}\``, false)

      return message.channel
        .send(Embed)
    }

    const name = args[0].toLowerCase();
    const command =
      commands.get(name) ||
      commands.find((c) => c.aliases && c.aliases.includes(name));

    if (!command) return message.reply("Нет такой команды!");

    const Embed = new MessageEmbed().setTitle(command.name);

    if (command.aliases) Embed.addField("Aliases", command.aliases.join(", "));
    if (command.category) Embed.addField("Category", command.category);
    if (command.description) Embed.addField("Description", command.description);
    if (command.usage)
      Embed.addField("Usage", `\`${prefix}${command.name} ${command.usage}\``);

    message.channel.send(Embed);
  },
};