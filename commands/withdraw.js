const { MessageEmbed } = require('discord.js')
module.exports = {
    name: "снять",
    category: "Экономика",
    description: "Забрать из банка деньги.",
    usage: "<сумма>",
    args: true,
    guildOnly: true,
    async execute (message, args, mongoCurrency) {
        if(isNaN(args[0])) return message.reply("Сумма должна быть цифрой.")
        if(args[0] < 1) return message.reply("Сумма должна быть больше 0.")
        let user = await mongoCurrency.findUser(message.author.id, message.guild.id);
        if(!user) return mongoCurrency.createUser(message.author.id, message.guild.id), message.reply("Вас нету в базе данных, поэтому я создал ваш профиль. Вам нечего переводить в банк.");
        const deposited = await mongoCurrency.withdraw(message.author.id, message.guild.id, args[0])
        const Embed = new MessageEmbed()
        .setAuthor(message.author.tag)
        .setDescription(`Вы забрали из банка ${deposited}₽`)
        return message.channel.send(Embed)
    }
}