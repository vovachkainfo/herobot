const { MessageEmbed } = require('discord.js')
module.exports = {
    name: "перевести",
    category: "Экономика",
    description: "Перевести кому нибудь деньги.",
    usage: "<пользователь> <сумма>",
    args: true,
    guildOnly: true,
    async execute (message, args, mongoCurrency) {
        if(!message.mentions.users.first()) return message.reply("Пожалуйсте, упомяните пользователя, которому вы хотите перевести деньги!")
        const target = message.mentions.users.first()
        if(!args[1]) return message.reply("Укажите сумму!")
        if(isNaN(args[1])) return message.reply("Укажите цифру в качестве суммы!")
        if(args[1] < 1) return message.reply("Укажите сумму больше 0!")
        let user = await mongoCurrency.findUser(message.author.id, message.guild.id);
        if (args[1] > user.coinsInWallet) return message.reply("У вас недостаточно денег чтобы перевести!")
        await mongoCurrency.giveCoins(target.id, message.guild.id, args[1])
        await mongoCurrency.deductCoins(message.author.id, message.guild.id, args[1])
        const Embed = new MessageEmbed()
        .setAuthor(message.author.tag)
        .addField("Перевод", `Вы успешно перевели ${target.tag} ${args[1]}₽`)
        return message.channel.send(Embed)
    }
}