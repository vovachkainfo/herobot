module.exports = {
    name: "бан",
    category: "Администрация",
    usage: "<юзер> <дней> [причина]",
    description: "Забанить человека на сервере. \n Поставьте `<дни>` в 0 для пермача",
    args: true,
    guildOnly: true,
    permission: "BAN_MEMBERS",
    execute(message, args) {
        const member = message.guild.member(message.mentions.users.first());
  
        if (!member)
          return message.channel.send(
            "Я не нашел этого пользователя!"
          );
    
        if (!args[1])
          return message.channel.send(
            `Пожалуйста, укажите, насколько дней забанить ${member.user.tag}.`
          );
    
        const matches = args[1].match(/(\d+)/);
        if (!matches)
          return message.channel.send(
            `Пожалуйста, используйте цифры, чтобы указать насколько забанить ${member.user.tag}.`
          );
    
        let days;
        if (matches[0] == 0) {
          days = `навсегда`;
        } else if (matches[0] == 1) {
            days = `день`
        } else if (matches[0] > 2 && matches[0] < 5) {
            days = `дня`
        }
        else {
          days = ` ${matches[0]} дней`;
        }
    
        if (!args[2])
          return member
            .ban({ days: matches[0] })
            .then(() => {
              message.reply(`✅ Забанил ${member.user.tag} ${matches[0] == 0 ? "" : "на"}${days}.`);
            })
            .catch(() => {
              message.channel.send(
                `❌ Can't ban ${member.user.tag}.`
              );
              console.error();
            });
    
        let msgArgs = args.slice(2).join(" ");
        member
          .ban({ days: matches[0], reason: msgArgs })
          .then(() => {
            message.reply(
              `✅ Забанил ${member.user.tag} за "${msgArgs}" ${matches[0] == 0 ? "" : "на"}${days}.`
            );
          })
          .catch(() => {
            message.channel.send(
              `❌ Не могу забанить ${member.user.tag}.`
            );
            console.error();
          });
    },
  };