const fetch = require('node-fetch')
const { MessageAttachment } = require('discord.js')
module.exports = {
    name: "вайфу",
    category: "NSFW",
    description: "Получить NSFW картинку с вайфу.",
    execute(message, args) {
        if (!message.channel.nsfw) return message.reply("используйте эту команду только в NSFW-каналах!")
        fetch('https://waifu.pics/api/nsfw/waifu')
            .then(res => res.json())
            .then(json => message.channel.send(new MessageAttachment(json.url)))
    },
  };