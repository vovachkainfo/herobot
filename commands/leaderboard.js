const Levels = require('discord-xp-firebase')
const { MessageEmbed } = require("discord.js");
module.exports = {
    name: "таблица",
    category: "Уровни",
    description: "Таблица лидеров по уровням.",
    aliases: ["лб"],
    args: false,
    guildOnly: true,
    async execute(message, args) {
        if(!message.guild) return;
        const rawLeaderboard = await Levels.fetchLeaderboard(message.guild.id, 10); // We grab top 10 users with most xp in the current server.
 
        if (rawLeaderboard.length < 1) return reply("В таблице лидеров пока никого..");
         
        const leaderboard = await Levels.computeLeaderboard(client, rawLeaderboard, true); // We process the leaderboard.
         
        const lb = leaderboard.map(e => `${e.position}. <@${e.userID}>\nУровень: ${e.level}\nXP: ${e.xp.toLocaleString()}`); // We map the outputs.
         
        const Embed = new MessageEmbed()
        .setTitle(`Таблица лидеров сервера`)
        .addField(`Уровни:`, lb, true)
        .setFooter(`Запросил ${message.author.username}`)
        message.channel.send(Embed)
    }
}