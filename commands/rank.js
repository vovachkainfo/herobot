const Levels = require('discord-xp-firebase')
const Discord = require("discord.js");
module.exports = {
    name: "ранг",
    category: "Уровни",
    description: "Показывает ваш уровень",
    aliases: ["р"],
    args: false,
    guildOnly: true,
    async execute(message, args) {
        if(!message.guild) return;
        const target = message.mentions.users.first() || message.author; // Grab the target.
        const avatar = target.displayAvatarURL({ format: 'png' });
        const user = await Levels.fetch(target.id, message.guild.id, true); // Selects the target from the database.
        if(target.bot == true) return message.reply(`Данныый пользователь - бот.`)
 
        if (!user) return Levels.createUser(target.id, message.guild.id).then(a => {Levels.appendXp(target.id, message.guild.id, 1)})
        const xpRequired = Levels.xpFor(Number(user.level)+1)
        const canvacord = require("canvacord");
        const rank = new canvacord.Rank()
            .setAvatar(avatar)
            .setCurrentXP(user.xp)
            .setRequiredXP(xpRequired)
            .setLevel(user.level)
            .setRank(user.position)
            .setStatus(target.presence.status, true)
            .setProgressBar("#FFFFFF", "COLOR")
            .setUsername(target.username)
            .setDiscriminator(target.discriminator)
         
        const img = await rank.build()
        
        const attachment = new Discord.MessageAttachment(img, "RankCard.png");
        message.channel.send(attachment);
    }
}