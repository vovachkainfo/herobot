const { MessageEmbed } = require('discord.js')
module.exports = {
    name: "работать",
    category: "Экономика",
    description: "Работать и получать деньги. Такова жизнь ¯\\_(ツ)_/¯",
    args: false,
    guildOnly: true,
    async execute (message, args, mongoCurrency, db) {
        if (message.author.workedToday == true) return;
        const randomMoney = Math.floor(Math.random() * 50) + 20
        await mongoCurrency.giveCoins(message.author.id, message.guild.id, randomMoney)
        const Embed = new MessageEmbed()
        .setAuthor(message.author.tag)
        .setDescription(`Вы работали и получили ${randomMoney}₽`)
        message.channel.send(Embed)
        message.author.workedToday = true
        setTimeout(function () {message.author.workedToday = false}, 86400000)
    }
}