module.exports = {
    name: "пинг",
    category: "Информация",
    description: "Пинг бота.",
    execute(message, args) {
        const { ws } = message.client;
        message.channel.send(`Понг! Пинг: ${ws.ping}мс`);
    },
  };