const { MessageEmbed } = require('discord.js')
module.exports = {
    name: "положить",
    category: "Экономика",
    description: "Положить на банк деньни из кошелька.",
    usage: "<сумма>",
    args: true,
    guildOnly: true,
    async execute (message, args, mongoCurrency) {
        if(isNaN(args[0])) return message.reply("Сумма должна быть цифрой.")
        if(args[0] < 1) return message.reply("Сумма должна быть больше 0.")
        let user = await mongoCurrency.findUser(message.author.id, message.guild.id);
        if(!user) return mongoCurrency.createUser(message.author.id, message.guild.id), message.reply("Вас нету в базе данных, поэтому я создал ваш профиль. Вам нечего переводить в банк.");
        if(args[0] > user.bankSpace) return message.reply("Сумма должна быть не больше вашего места в банке.")
        const deposited = await mongoCurrency.deposit(message.author.id, message.guild.id, args[0])
        const Embed = new MessageEmbed()
        .setAuthor(message.author.tag)
        .setDescription(`Вы положили в банк ${deposited}₽`)
        return message.channel.send(Embed)
    }
}