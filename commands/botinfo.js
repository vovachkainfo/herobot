const { MessageEmbed } = require("discord.js");
const os = require("os");
const { version, dependencies } = require("../package.json");
const { owners } = require("../config.json")
module.exports = {
  name: "инфа",
  description: "Информация о боте.",
  aliases: ["botinfo", "bot", "links"],
  category: "Информация",
  execute(message, args) {
    const client = message.client;

    let serverCount;
    client.guilds.cache.tap((coll) => {
      serverCount = coll.size;
    });

    let channelCount;
    client.channels.cache.tap((coll) => {
      channelCount = coll.size;
    });

    let userCount;
    client.users.cache.tap((coll) => {
      userCount = coll.size;
    });

    let totalSeconds = client.uptime / 1000;
    let days = Math.floor(totalSeconds / 86400);
    let hours = Math.floor(totalSeconds / 3600);
    totalSeconds %= 3600;
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;

    const memory = process.memoryUsage().heapUsed / 1048576; // 1024*1024
    let memoryUsage;
    if (memory >= 1024) memoryUsage = `${(memory / 1024).toFixed(2)}ГБ`;
    else memoryUsage = `${memory.toFixed(2)}МБ`;

    const Embed = new MessageEmbed()
      .setAuthor(
        `Автор: ${client.users.cache.get(owners[1]).tag}, владелец: ${client.users.cache.get(owners[0]).tag}`,
      )
      .setTitle("Информация о боте")
      .setFooter(`HeroBot, версия: ${version}`)
      .setTimestamp()
      .addFields([
        { name: "ОС", value: `${os.type()} (${os.release()})`, inline: false },
        {
          name: "ЦП",
          value: `${os.cpus()[0].model}`,
          inline: true,
        },
        {
          name: "ОЗУ",
          value: `${(os.totalmem() / 1024 / 1024 / 1024 ).toFixed(0)} ГБ`,
          inline: true,
        },
        {
          name: "Использованная ОЗУ",
          value: memoryUsage,
          inline: true,
        },
        {
          name: "Аптайм",
          value: `${days}д, ${hours}ч, ${minutes}м, ${Math.round(seconds)}с`,
          inline: true,
        },
        {
          name: "Пинг",
          value: `${Math.round(client.ws.ping)}мс`,
          inline: true,
        },
      ]);

    message.channel.send(Embed);
}}