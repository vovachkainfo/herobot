const { MessageEmbed } = require('discord.js')
module.exports = {
    name: "баланс",
    category: "Экономика",
    description: "Узнать ваш/чей-то баланс.",
    aliases: ["бал"],
    usage: "[упоминание]",
    args: false,
    guildOnly: true,
    async execute (message, args, mongoCurrency) {
        const target = message.mentions.users.first() || message.author;
        let user = await mongoCurrency.findUser(target.id, message.guild.id);
        if(user.coinsInBank == undefined) await mongoCurrency.createUser(target.id, message.guild.id)
        const Embed = new MessageEmbed()
        .setAuthor(target.tag)
        .addField(`Баланс ${target.username}:`, `Кошелек: ${user.coinsInWallet}₽, банк: ${user.coinsInBank}₽/${user.bankSpace}₽`)
        return message.channel.send(Embed)
    }
}