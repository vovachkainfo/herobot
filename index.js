// Modules initialization
const fs = require("fs");
const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json")
const firebaseConfig = require("./serviceAccount.json")
const Levels = require('discord-xp-firebase')
Levels.setAccount(firebaseConfig)
const mongoCurrency = require('discord-mongo-currency-fork');
mongoCurrency.connect('mongodb+srv://admin:18amazona@cluster0.fngea.mongodb.net/money?retryWrites=true&w=majority')
global.client = client   
const firebase = require("firebase/app");
const admin = require("firebase-admin");
let db = admin.firestore(); 

// Loading Commands
client.commands = new Discord.Collection();
const commandFiles = fs
  .readdirSync("./commands")
  .filter((file) => file.endsWith(".js"));
for (const file of commandFiles) {
  const command = require(`./commands/${file}`);

  client.commands.set(command.name, command);
}
// On Ready
client.once("ready", async () => {
    console.log(`Logged in as: ${client.user.tag}`);
    client.user.setActivity(`за сервером Hero | Префикс ${config.prefix}`, { type: 'WATCHING' });
    const channel = await client.channels.fetch("810390634237394944")
    if (process.argv.includes("--dev")) return console.log("In dev mod, not sending login messsage.")
    if (!process.argv.includes("--dev")) return channel.send("HeroBot has started!")
  });
client.on("message", async (message) => {
    if (message.author.bot) return;
    if (message.mentions.users.first()) {
      if(client.users.cache.get(message.mentions.users.first().id).presence.activities != null && client.users.cache.get(message.mentions.users.first().id).presence.activities[0].type == "PLAYING" && message.author.id != message.mentions.users.first().id && message.guild.members.cache.get(message.mentions.users.first().id)._roles.includes("715599607094182000")) return message.reply("АРА ТЫ ЧО? НА СТАТУС СМОТРИ")
    }
    if (message.content.includes("@everyone")) return message.reply("АРА ТЫ ЧО?")
    if (message.content.includes("@here")) return message.reply("АРА ТЫ ЧО?")
    if(message.guild) {
      if(!message.content.startsWith(config.prefix)) {
        if (message.author.bot) return;
          
        const randomAmountOfXp = Math.floor(Math.random() * 29) + 1; // Min 1, Max 30
        const hasLeveledUp = await Levels.appendXp(message.author.id, message.guild.id, randomAmountOfXp);
        if (hasLeveledUp) {
          const user = await Levels.fetch(message.author.id, message.guild.id);
          message.channel.send(`${message.author}, поздравляю, у тебя новый уровень! Твой уровень: **${user.level}**. :tada:`);
        }}
    }
    const args = message.content.slice(config.prefix.length).trim().split(/ +/);
    const commandName = args.shift().toLowerCase();
    if(!message.content.toLowerCase().startsWith(config.prefix)) return;
    const command =
    client.commands.get(commandName) || client.commands.find(
      (cmd) => cmd.aliases && cmd.aliases.includes(commandName)
    );
    if (!command) return;
    if (command.args && !args.length) {
        reply = `Пожалуйста, дайте аргументы для команды!`;
  
      if (command.usage) {
        reply += `\nИспользование: \`${config.prefix}${command.name} ${command.usage}\``;
      }
      return message.reply(reply)
    }
    if (command.permission) {
        if (
          !message.member.hasPermission(command.permission)
        ) return message.reply(
            `У вас нету прав!\nВам нужно право \`${command.permission}\` чтобы использовать эту команду!`)
      }
    if (command.ownerOnly && !config.owners.includes(message.author.id)) return message.reply("Вы не владелец бота и не можете использовать данную команду!")
      try {
        command.execute(message, args, mongoCurrency, db);
      } catch (error) {
        console.error(error);
        message.reply("There was an error when executing the command!");
      }
})
client.on('voiceStateUpdate', (oldState, newState) => {
    if (oldState.channelID == null && newState.channelID == "810188285266624552") {
      let everyone = newState.guild.roles.cache.find(r => r.name === "@everyone");
      setTimeout(a => {
      client.guilds.cache.get("692326810595360861").channels.create(newState.member.user.username, { type: "voice", parent: "810188284495659048" , permissionOverwrites: [ {
        id: everyone.id,
        deny: ['VIEW_CHANNEL'],
      }, {
        id: newState.member.id,
        allow: ['VIEW_CHANNEL', 'USE_VAD', 'CONNECT', 'SPEAK', 'MANAGE_CHANNELS']
      }]}).then((NewChannel) => {
        newState.member.voice.setChannel(NewChannel)
      })}, 3000)
    }
    if (![ "810187983022063617", "810189857920909332", "810188285266624552" ].includes(oldState.channelID) && newState.channelID == null && !oldState.channel.members.size > 0) {
      oldState.channel.delete()
    }
    if(oldState.channelID != null && newState.channelID != null && newState.channelID != oldState.channelID && ![ "810187983022063617", "810189857920909332", "810188285266624552" ].includes(oldState.channelID) && !oldState.channel.members.size > 0) {
      oldState.channel.delete()
  }
});
client.on('guildMemberAdd', async member => {
  const channel = member.guild.channels.cache.get("810782526792269884")
  var role = member.guild.roles.cache.get("692382863357771786");
      member.roles.add(role)
  mongoCurrency.createUser(member.user.id, member.guild.id)
const { MessageAttachment } = require("discord.js");

const canvas = require("discord-canvas"),
  welcomeCanvas = new canvas.Welcome();

let image = await welcomeCanvas
  .setUsername(member.user.username)
  .setDiscriminator(member.user.discriminator)
  .setMemberCount(member.guild.memberCount)
  .setGuildName(member.guild.name)
  .setAvatar(member.user.displayAvatarURL({ format: "png" }))
  .setColor("border", "#E6E6FA")
  .setColor("username-box", "#E6E6FA")
  .setColor("discriminator-box", "#E6E6FA")
  .setColor("message-box", "#E6E6FA")
  .setColor("title", "#E6E6FA")
  .setColor("avatar", "#E6E6FA")
  .setBackground("./wallpaper.jpg")
  .toAttachment();

let attachment = new MessageAttachment(image.toBuffer(), "welcome-image.png");

return channel.send(attachment);
});
client.on('guildMemberRemove', async member => {
  const channel = client.channels.cache.get("810782526792269884")
  const { MessageAttachment } = require("discord.js");
  mongoCurrency.deleteUser(member.user.id, member.guild.id)

const canvas = require("discord-canvas"),
  welcomeCanvas = new canvas.Goodbye();

let image = await welcomeCanvas
  .setUsername(member.user.username)
  .setDiscriminator(member.user.discriminator)
  .setMemberCount(member.guild.memberCount)
  .setGuildName(member.guild.name)
  .setAvatar(member.user.displayAvatarURL({ format: "png" }))
  .setColor("border", "#E6E6FA")
  .setColor("username-box", "#E6E6FA")
  .setColor("discriminator-box", "#E6E6FA")
  .setColor("message-box", "#E6E6FA")
  .setColor("title", "#E6E6FA")
  .setColor("avatar", "#E6E6FA")
  .setBackground("./wallpaper.jpg")
  .toAttachment();

let attachment = new MessageAttachment(image.toBuffer(), "welcome-image.png");

return channel.send(attachment);
})
client.login(config.token)
